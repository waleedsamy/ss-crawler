import com.typesafe.config.ConfigFactory
import io.github.waleedsamy.sscrawler.crawler.{ CrawlerController, CrawlerUriHandler }
import io.github.waleedsamy.sscrawler.search.{ SearchController, SearchUriHandler }
import io.github.waleedsamy.sscrawler.service.{ InMemoryStorage, SimplestSearchInTextEver, SimpleIndexer }
import javax.servlet.ServletContext
import org.scalatra._

import scala.collection.mutable
import scala.concurrent.ExecutionContext

class ScalatraBootstrap extends LifeCycle {

  // Scalatra suggests importing Akka Actors to use executor = actorSystem.dispatcher.
  // Since we do not use the actors, we can skip this dependency and use the default
  // ExecutionContext implementation from scala.concurrency, based on ForkJoinPool.
  protected implicit def executionContext = ExecutionContext.Implicits.global

  type DateBase[URL, Page] = mutable.Map[URL, String]

  override def init(context: ServletContext) {
    val config = ConfigFactory.load()

    val storage = new InMemoryStorage
    val indexer = new SimpleIndexer(storage)
    val searchEngine = new SimplestSearchInTextEver(storage, executionContext)

    val crawlerController = new CrawlerController(indexer, config, executionContext)
    val searchController = new SearchController(searchEngine, config, executionContext)

    context.mount(new CrawlerUriHandler(executionContext, crawlerController), CrawlerUriHandler.Path)
    context.mount(new SearchUriHandler(executionContext, searchController), SearchUriHandler.Path)
  }
}
