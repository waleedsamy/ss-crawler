package io.github.waleedsamy.sscrawler.shared

import java.time.LocalDateTime

object DateTimeUtil {
  def reverseDateTimeOrdering: Ordering[LocalDateTime] =
    Ordering.fromLessThan((a, b) => b isBefore a)
}
