package io.github.waleedsamy.sscrawler.model

import java.time._
import java.util.UUID

import org.json4s.jackson.JsonMethods._
import org.json4s.{ DefaultFormats, Formats, _ }

case class IndexedPage(
  @transient id: String = UUID.randomUUID().toString,
  @transient host: String,
  @transient protocol: String = "https",
  uri: String,
  @transient body: Content,
  @transient links: List[String] = List.empty,
  @transient updatedTime: Long = Instant.now.toEpochMilli
)

object IndexedPage {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  def readOpt(rawJson: String): Option[IndexedPage] = parse(rawJson).extractOpt[IndexedPage]
}