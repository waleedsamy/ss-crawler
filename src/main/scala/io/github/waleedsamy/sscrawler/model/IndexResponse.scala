package io.github.waleedsamy.sscrawler.model

import java.time._

import org.json4s.jackson.JsonMethods.parse
import org.json4s.{ DefaultFormats, Formats }

case class IndexResponse(indexRequest: IndexRequest, time: Long = Instant.now.toEpochMilli)

object IndexResponse {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  def readOpt(rawJson: String): Option[IndexResponse] = parse(rawJson).extractOpt[IndexResponse]
}
