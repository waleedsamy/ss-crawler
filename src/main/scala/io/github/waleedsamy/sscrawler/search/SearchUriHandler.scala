package io.github.waleedsamy.sscrawler.search

import io.github.waleedsamy.sscrawler.model.SearchRequest
import io.github.waleedsamy.sscrawler.{ CrawlerServiceStack, CrawlerUriHandlerImpl }
import org.json4s.{ DefaultFormats, Formats }
import org.scalatra.Ok

import scala.concurrent.ExecutionContext
import scala.util.Try

class SearchUriHandler(
    executionContext: ExecutionContext,
    controller: SearchController
) extends CrawlerServiceStack with CrawlerUriHandlerImpl {

  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  protected implicit def executor = executionContext

  getLogRecover("/") {
    val query = params("q")
    val limit = Try { params("limit").toInt }.getOrElse(10)

    val searchRequest = SearchRequest(query, limit)

    controller.search(searchRequest).map(Ok(_))
  }
}

object SearchUriHandler {
  val Path = "/search"
}