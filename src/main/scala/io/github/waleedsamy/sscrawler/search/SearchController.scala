package io.github.waleedsamy.sscrawler.search

import com.typesafe.config.Config
import io.github.waleedsamy.sscrawler.model.{ SearchRequest, SearchResult }
import io.github.waleedsamy.sscrawler.service.SearchEngine
import io.github.waleedsamy.sscrawler.shared.{ DateTimeUtil, FutureUtil, Logging }
import org.json4s.{ DefaultFormats, Formats }

import scala.concurrent.{ ExecutionContext, Future }

class SearchController(
    searchEngine: SearchEngine,
    appConfig: Config,
    executionContext: ExecutionContext
) extends FutureUtil with Logging {

  implicit lazy val jsonFormats: Formats = DefaultFormats

  implicit def executor = executionContext

  def _appConfig = appConfig

  def search(searchRequest: SearchRequest): Future[Seq[SearchResult]] = {
    implicit def ordering = DateTimeUtil.reverseDateTimeOrdering
    searchEngine.search(searchRequest)
  }

}