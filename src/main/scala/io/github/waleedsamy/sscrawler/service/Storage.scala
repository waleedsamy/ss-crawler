package io.github.waleedsamy.sscrawler.service

import java.net.URL

import io.github.waleedsamy.sscrawler.model.IndexedPage
import io.github.waleedsamy.sscrawler.shared.Logging

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

trait Storage {
  def store(page: IndexedPage): Option[IndexedPage]
  def find(uri: URL): Option[IndexedPage]
  def pages: Iterator[IndexedPage]
}

/**
 * Store pages in hashMap with the host as a Key, pages get prepended, so you get the fresh result always
 * {class @SearchEngine} will pages iterator elements till the search limit reached
 */
class InMemoryStorage extends Storage with Logging {
  type Host = String
  val db: mutable.Map[Host, ListBuffer[IndexedPage]] = mutable.Map()

  override def store(page: IndexedPage): Option[IndexedPage] = {
    db.get(page.host) match {
      case Some(xs: ListBuffer[IndexedPage]) => {
        xs.prepend(page)
        db.update(page.host, xs)
      }
      case None => db.put(page.host, ListBuffer(page))
    }

    Some(page)
  }

  override def find(uri: URL): Option[IndexedPage] = {
    db.values.flatMap { pages =>
      pages.find(_.uri == uri.toString)
    }.headOption
  }

  override def pages: Iterator[IndexedPage] = db.values.flatMap(_.iterator).toIterator
}
