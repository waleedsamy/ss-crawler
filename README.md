# ss-crawler #
 SS stand for simple stupid, and it's 2-3 hours work
 
## Build & Run ##

```sh

$ cd ss-crawler
$ sbt
> jetty:start
```


###### Provides two endpoints

   * submit a site to be indexed
       ```bash
       
       curl -X POST http://localhost:8080/index -H "Content-Type: application/json" -d '{"site":"https://edition.cnn.com/sport"}'
       curl -X POST http://localhost:8080/index -H "Content-Type: application/json" -d '{"site":"https://www.dw.com"}'
       curl -X POST http://localhost:8080/index -H "Content-Type: application/json" -d '{"site":"http://www.africanews.com"}'
       ```
   * search in indexed sites
        ```bash
        
        curl http://localhost:8080/search?q=egypt
        curl http://localhost:8080/search?q=nile
        curl http://localhost:8080/search?q=salah&limit=12
        curl http://localhost:8080/search\?q\=save%20coalition
        ```
      - can limit the result with `limit` query param
      - you always getting the latest data about the thing you looking for
      
* I was on rush to write an acceptance test for `/index` endpoint but when running `sbt test` it tells that it's Empty test suite (probably there are a missing interface to extend, but now time to know which)
* Use [Jq](https://stedolan.github.io/jq) for beautiful view of curl queries